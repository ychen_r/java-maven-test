def jobScript(String testEnv, boolean notify) {
    String stageStatus = 'FAILURE'

    catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
        sh 'mvn -B -DskipTests clean package'
        stageStatus = 'SUCCESS'
    }
    
    stageStatus = "FAILURE"

    if (stageStatus == 'FAILURE' && notify) {
        echo "The stageStatus is: ${stageStatus}. The job name is: ${env.JOB_NAME}."
        sendNotification("jobName", "cusMessage", "FAILURE")
        sendNotification1(env.JOB_NAME, "dev0", "UNSTABLE")
    }

}

def sendNotification(String jobName = env.JOB_NAME, String cusMessage, String result = "FAILURE") {
    echo "${jobName}, ${env.BUILD_NUMBER}, ${cusMessage}, ${result}"
}

def sendNotification1(String jobName = env.JOB_NAME, String cusMessage, String result = "FAILURE") {
    echo "${jobName}, ${env.BUILD_NUMBER}, ${cusMessage}, ${result}"
}

return this